<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'merchant_id' => 'required',
            'name' => 'required',
            'price' => 'required|numeric',
            'sales_price' => 'required|numeric',
            'app_price' => 'required|numeric',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg',
            'stock' => 'required|numeric',
            'description' => 'required',
        ];
    }
}
