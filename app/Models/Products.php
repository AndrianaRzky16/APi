<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Updater;


class Products extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    protected $primaryKey = 'id';

    protected $fillable = [
        'merchant_id',
        'name',
        'price',
        'sales_price',
        'app_price',
        'image',
        'description',
        'stock',
    ];

    public function img_url($size = null)
    {
        $path = 'files/upload/product/' . (!is_null($size) ? $size . '/' : '');
        $url = asset('dashboard/media/users/default.jpg');

        if (File::exists($path . $this->image) && $this->image != '') {
            $url = asset($path . $this->image);
        }
        return $url;
    }

    // public function unit()
    // {
    //     return $this->belongsTo(Unit::class, 'unit_id');
    // }

    // public function category()
    // {
    //     return $this->belongsTo(Category::class, 'category_id');
    // }

    public function merchant()
    {
        return $this->belongsTo('App\Models\Merchant', 'merchant_id', 'id');
    }
}
