<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $table = 'addresses';
    protected $primaryKey = 'id';

    protected $fillable = [
        'customer_id',
        'name',
        'phone_number',
        'address',
        'province_id',
        'regency_id',
        'district_id',
        'village_id',
    ];
}
